#include "widget.h"
#include "TreeModel.h"
#include <QApplication>
#include <QTreeView>
#include <QSplitter>
#include <QStringList>
#include <QStringListModel>
#include <QFileSystemModel>
#include <QStandardItemModel>
#include <QStandardItem>

QList<QStandardItem *>  prepareRow(const QString &first,
                                                const QString &second,
                                                const QString &third)
{
    QList<QStandardItem *> rowItems;
    rowItems << new QStandardItem(first);
    rowItems << new QStandardItem(second);
    rowItems << new QStandardItem(third);
    return rowItems;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QSplitter splitter;
    QTreeView treeView1,treeView2;
    TreeModel treeModel;

    treeView1.setModel( &treeModel );

    RevertProxyModel *proxyModel = new RevertProxyModel(0);
    proxyModel->setSourceModel( &treeModel );


    treeView2.setModel( proxyModel );
    splitter.addWidget( &treeView1 );
    splitter.addWidget( &treeView2 );
    splitter.show();
    return app.exec();
 }
