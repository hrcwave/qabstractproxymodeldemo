#-------------------------------------------------
#
# Project created by QtCreator 2015-04-10T14:32:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    treemodel.cpp \
    widget.cpp

HEADERS  += mainwindow.h \
    treemodel.h \
    widget.h

FORMS    +=
